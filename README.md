# sis8300drv conda recipe

Home: https://gitlab.esss.lu.se/epics-modules/sis8300drv.git

Package license: GNU AGPL

Recipe license: BSD 3-Clause

Summary: EPICS sis8300drv module
